// flutter build apk --split-per-abi
// flutter build appbundle
// µ
import 'package:flutter/material.dart';
import 'package:leki_u_dzieci/src/screens/about.dart';
import 'package:leki_u_dzieci/src/screens/detailsScreens/blade.dart';
import 'package:leki_u_dzieci/src/screens/detailsScreens/breathing.dart';
import 'package:leki_u_dzieci/src/screens/detailsScreens/drugsDetails.dart';
import 'package:leki_u_dzieci/src/screens/detailsScreens/glukoze.dart';
import 'package:leki_u_dzieci/src/screens/detailsScreens/intubacja.dart';
import 'package:leki_u_dzieci/src/screens/detailsScreens/kardioversion.dart';
import 'package:leki_u_dzieci/src/screens/detailsScreens/maska.dart';
import 'package:leki_u_dzieci/src/screens/detailsScreens/pressure.dart';
import 'package:leki_u_dzieci/src/screens/detailsScreens/pulseScreen.dart';
import 'package:leki_u_dzieci/src/screens/weightScreen.dart';

import 'src/screens/ageGrid.dart';
import 'src/screens/drugListScreen.dart';
import 'src/screens/tabsScreen.dart';
import 'src/screens/tips/big.dart';
import 'src/screens/tips/defibrilation.dart';
import 'src/screens/tips/kedScreen.dart';
import 'src/screens/tips/konikopunkcja.dart';
import 'src/screens/tips/newBorn.dart';
import 'src/screens/tips/odma.dart';
import 'src/screens/tips/pedipac.dart';
import 'src/screens/tips/wklucie.dart';
import 'src/screens/tipsList.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
        accentColor: Colors.amber,
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
              body1: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
                fontSize: 18,
              ),
              body2: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              title: TextStyle(
                fontSize: 18,
                fontFamily: 'RobotoCondensed',
                fontWeight: FontWeight.bold,
              ),
            ),
      ),
      initialRoute: TabsScreen.id,
      routes: {
        NewBorn.id: (context) => NewBorn(),
        AgeGridView.id: (context) => AgeGridView(),
        DrugListScreen.id: (context) => DrugListScreen(),
        TabsScreen.id: (context) => TabsScreen(),
        TipsList.id: (context) => TipsList(),
        KedScreen.id: (context) => KedScreen(),
        WklucieScreen.id: (context) => WklucieScreen(),
        BigScreen.id: (context) => BigScreen(),
        Konikopunkcja.id: (context) => Konikopunkcja(),
        Odma.id: (context) => Odma(),
        Defibrilation.id: (context) => Defibrilation(),
        Pedipac.id: (context) => Pedipac(),
        WeightScreen.id: (context) => WeightScreen(),
        AboutScreen.id: (context) => AboutScreen(),
        PressureScreen.id: (context) => PressureScreen(),
        BreathingScreen.id: (context) => BreathingScreen(),
        PulseScreen.id: (context) => PulseScreen(),
        GlukozeScreen.id: (context) => GlukozeScreen(),
        IntubacjaScreen.id: (context) => IntubacjaScreen(),
        BladeScreen.id: (context) => BladeScreen(),
        MaskaScreen.id: (context) => MaskaScreen(),
        KardioversionScreen.id: (context) => KardioversionScreen(),
        DrugDetailsScreen.id: (context) => DrugDetailsScreen(),
      },
    );
  }
}
