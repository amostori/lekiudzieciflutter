import 'package:flutter/material.dart';

import 'ageModel.dart';

const AGES_DATA = const [
  Age(
    title: '< 1 m.ż.',
    color: Colors.red,
    wiek: '1',
  ),
  Age(
    title: '1 m.ż.',
    color: Colors.orange,
    wiek: '2',
  ),
  Age(
    title: '2 m.ż.',
    color: Colors.blue,
    wiek: '3',
  ),
  Age(
    title: '3 m.ż.',
    color: Colors.amber,
    wiek: '4',
  ),
  Age(
    title: '4 m.ż.',
    color: Colors.green,
    wiek: '5',
  ),
  Age(
    title: '5 m.ż.',
    color: Colors.lightBlue,
    wiek: '6',
  ),
  Age(
    title: '6 m.ż.',
    color: Colors.lightGreen,
    wiek: '7',
  ),
  Age(
    title: '7 m.ż.',
    color: Colors.pink,
    wiek: '8',
  ),
  Age(
    title: '8 m.ż.',
    color: Colors.teal,
    wiek: '9',
  ),
  Age(
    title: '9 m.ż.',
    color: Colors.purple,
    wiek: '10',
  ),
  Age(
    title: '10 m.ż.',
    color: Colors.red,
    wiek: '11',
  ),
  Age(
    title: '11 m.ż.',
    color: Colors.orange,
    wiek: '12',
  ),
  Age(
    title: '12 m.ż.',
    color: Colors.blue,
    wiek: '13',
  ),
  Age(
    title: '18 m.ż.',
    color: Colors.amber,
    wiek: '14',
  ),
  Age(
    title: '2 lata',
    color: Colors.green,
    wiek: '15',
  ),
  Age(
    title: '3 lata',
    color: Colors.lightBlue,
    wiek: '16',
  ),
  Age(
    title: '4 lata',
    color: Colors.lightGreen,
    wiek: '17',
  ),
  Age(
    title: '5 lat',
    color: Colors.pink,
    wiek: '18',
  ),
  Age(
    title: '6 lat',
    color: Colors.teal,
    wiek: '19',
  ),
  Age(
    title: '7 lat',
    color: Colors.pink,
    wiek: '20',
  ),
  Age(
    title: '8 lat',
    color: Colors.teal,
    wiek: '21',
  ),
  Age(
    title: '9 lat',
    color: Colors.purple,
    wiek: '22',
  ),
  Age(
    title: '10 lat',
    color: Colors.red,
    wiek: '23',
  ),
  Age(
    title: '11 lat',
    color: Colors.orange,
    wiek: '24',
  ),
  Age(
    title: '12 lat',
    color: Colors.blue,
    wiek: '25',
  ),
  Age(
    title: '>12 lat',
    color: Colors.amber,
    wiek: '26',
  ),
];

/*
Age(
title: 'Świeżorodek',
color: Colors.purple,
wiek: '0',
swiezorodek: true,
),
Age(
title: 'Dziecko < 1 m.ż.',
color: Colors.red,
wiek: '1',
),
Age(
title: 'Dziecko 1 m.ż.',
color: Colors.orange,
wiek: '2',
),
Age(
title: 'Dziecko 2 m.ż.',
color: Colors.blue,
wiek: '3',
),
Age(
title: 'Dziecko 3 m.ż.',
color: Colors.amber,
wiek: '4',
),
Age(
title: 'Dziecko 4 m.ż.',
color: Colors.green,
wiek: '5',
),
Age(
title: 'Dziecko 5 m.ż.',
color: Colors.lightBlue,
wiek: '6',
),
Age(
title: 'Dziecko 6 m.ż.',
color: Colors.lightGreen,
wiek: '7',
),
Age(
title: 'Dziecko 7 m.ż.',
color: Colors.pink,
wiek: '8',
),
Age(
title: 'Dziecko 8 m.ż.',
color: Colors.teal,
wiek: '9',
),
Age(
title: 'Dziecko 9 m.ż.',
color: Colors.purple,
wiek: '10',
),
Age(
title: 'Dziecko 10 m.ż.',
color: Colors.red,
wiek: '11',
),
Age(
title: 'Dziecko 11 m.ż.',
color: Colors.orange,
wiek: '12',
),
Age(
title: 'Dziecko 12 m.ż.',
color: Colors.blue,
wiek: '13',
),
Age(
title: 'Dziecko 18 m.ż.',
color: Colors.amber,
wiek: '14',
),
Age(
title: 'Dziecko 2 lata',
color: Colors.green,
wiek: '15',
),
Age(
title: 'Dziecko 3 lata',
color: Colors.lightBlue,
wiek: '16',
),
Age(
title: 'Dziecko 4 lata',
color: Colors.lightGreen,
wiek: '17',
),
Age(
title: 'Dziecko 5 lat',
color: Colors.pink,
wiek: '18',
),
Age(
title: 'Dziecko 6 lat',
color: Colors.teal,
wiek: '19',
),
Age(
title: 'Dziecko 7 lat',
color: Colors.pink,
wiek: '20',
),
Age(
title: 'Dziecko 8 lat',
color: Colors.teal,
wiek: '21',
),
Age(
title: 'Dziecko 9 lat',
color: Colors.purple,
wiek: '22',
),
Age(
title: 'Dziecko 10 lat',
color: Colors.red,
wiek: '23',
),
Age(
title: 'Dziecko 11 lat',
color: Colors.orange,
wiek: '24',
),
Age(
title: 'Dziecko 12 lat',
color: Colors.blue,
wiek: '25',
),
Age(
title: 'Dziecko >12 lat',
color: Colors.amber,
wiek: '26',
),*/
