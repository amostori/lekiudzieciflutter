import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:leki_u_dzieci/src/screens/about.dart';
import 'package:leki_u_dzieci/src/screens/tips/big.dart';
import 'package:leki_u_dzieci/src/screens/tips/defibrilation.dart';
import 'package:leki_u_dzieci/src/screens/tips/kedScreen.dart';
import 'package:leki_u_dzieci/src/screens/tips/konikopunkcja.dart';
import 'package:leki_u_dzieci/src/screens/tips/newBorn.dart';
import 'package:leki_u_dzieci/src/screens/tips/odma.dart';
import 'package:leki_u_dzieci/src/screens/tips/pedipac.dart';
import 'package:leki_u_dzieci/src/screens/tips/wklucie.dart';

class MainDrawer extends StatelessWidget {
  Future<void> sendEmail() async {
    final Email email = Email(
      subject: 'Leki u dzieci',
      recipients: ['amostori@op.pl'],
    );
    String platformResponse;

    try {
      await FlutterEmailSender.send(email);
      platformResponse = 'success';
    } catch (error) {
      platformResponse = error.toString();
    }
    print('email platformResponse = $platformResponse');
  }

  @override
  Widget build(BuildContext context) {
    void goTo(String screen) {
      Navigator.of(context).pop();
      Navigator.of(context).pushNamed(screen);
    }

    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('images/drawer.jpg'), fit: BoxFit.cover),
            ),
            accountName: Text("Leki u dzieci"),
            accountEmail: Text("amostori@op.pl"),
            onDetailsPressed: sendEmail,
            currentAccountPicture: Image(
              image: AssetImage('images/ramiona.jpg'),
              fit: BoxFit.scaleDown,
            ),
          ),
          ListTile(
            onTap: () => goTo(AboutScreen.id),
            title: Text('O aplikacji'),
            trailing: Icon(Icons.chevron_right),
          ),
          ListTile(
            onTap: () => goTo(NewBorn.id),
            title: Text('Resuscytacja świeżorodka'),
            trailing: Icon(Icons.chevron_right),
          ),
          ListTile(
            onTap: () => goTo(KedScreen.id),
            title: Text('Zakładanie keda'),
            trailing: Icon(Icons.chevron_right),
          ),
          ListTile(
            onTap: () => goTo(WklucieScreen.id),
            title: Text('Zakładanie wkłucia dożylnego'),
            trailing: Icon(Icons.chevron_right),
          ),
          ListTile(
            onTap: () => goTo(BigScreen.id),
            title: Text('Wkłucie doszpikowe'),
            trailing: Icon(Icons.chevron_right),
          ),
          ListTile(
            onTap: () => goTo(Konikopunkcja.id),
            title: Text('Konikopunkcja'),
            trailing: Icon(Icons.chevron_right),
          ),
          ListTile(
            onTap: () => goTo(Odma.id),
            title: Text('Odma prężna'),
            trailing: Icon(Icons.chevron_right),
          ),
          ListTile(
            onTap: () => goTo(Defibrilation.id),
            title: Text('Defibrylacja u dzieci'),
            trailing: Icon(Icons.chevron_right),
          ),
          ListTile(
            onTap: () => goTo(Pedipac.id),
            title: Text('Pedipac'),
            trailing: Icon(Icons.chevron_right),
          ),
        ],
      ),
    );
  }
}
