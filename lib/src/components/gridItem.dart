import 'package:flutter/material.dart';
import 'package:leki_u_dzieci/src/screens/drugListScreen.dart';

class GridItem extends StatelessWidget {
  final String title;
  final Color color;
  final String wiek;
  final String weight;

  const GridItem({this.title, this.color, this.wiek, this.weight = '0'});

  void selectPage(BuildContext context) {
    Navigator.pushNamed(
      context,
      DrugListScreen.id,
      arguments: {'wiek': wiek, 'title': title, 'weight': weight},
    );
  }

  @override
  Widget build(BuildContext context) {
    /*Icon ageIcon;
    int temp = int.parse(wiek);
    if (temp < 15) {
      ageIcon = Icon(
        FontAwesomeIcons.baby,
        color: kTextColor,
      );
    } else if (temp > 14 && temp < 20) {
      ageIcon = Icon(
        FontAwesomeIcons.child,
        color: kTextColor,
      );
    } else {
      ageIcon = Icon(
        FontAwesomeIcons.graduationCap,
        color: kTextColor,
      );
    }*/
    return InkWell(
      onTap: () => selectPage(context),
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(8),
      child: Container(
        padding: const EdgeInsets.all(8),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                    fontFamily: 'RobotoCondensed',
                    fontWeight: FontWeight.bold,
                    fontSize: 28),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 8,
              ),
            ],
          ),
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              color.withOpacity(0.7),
              color,
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
          borderRadius: BorderRadius.circular(8),
        ),
      ),
    );
  }
}
