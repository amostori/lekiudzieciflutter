import 'package:flutter/material.dart';
import 'package:leki_u_dzieci/src/components/gridItem.dart';
import 'package:leki_u_dzieci/src/models/dataAges.dart';

class AgeGridView extends StatelessWidget {
  static const String id = 'AgeGridView';
  @override
  Widget build(BuildContext context) {
    return GridView(
      padding: const EdgeInsets.all(18),
      children: AGES_DATA
          .map((data) => GridItem(
                title: data.title,
                color: data.color,
                wiek: data.wiek,
              ))
          .toList(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200,
        childAspectRatio: 3 / 2,
        mainAxisSpacing: 18,
        crossAxisSpacing: 18,
      ),
    );
  }
}
