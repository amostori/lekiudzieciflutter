import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:leki_u_dzieci/src/screens/tips/big.dart';
import 'package:leki_u_dzieci/src/screens/tips/defibrilation.dart';
import 'package:leki_u_dzieci/src/screens/tips/kedScreen.dart';
import 'package:leki_u_dzieci/src/screens/tips/konikopunkcja.dart';
import 'package:leki_u_dzieci/src/screens/tips/newBorn.dart';
import 'package:leki_u_dzieci/src/screens/tips/odma.dart';
import 'package:leki_u_dzieci/src/screens/tips/pedipac.dart';
import 'package:leki_u_dzieci/src/screens/tips/wklucie.dart';

class TipsList extends StatelessWidget {
  static const String id = 'TipsList';

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        ListTile(
          trailing: Icon(FontAwesomeIcons.angleRight),
          title: Text('Reanimacja świeżorodka'),
          onTap: () => Navigator.of(context).pushNamed(NewBorn.id),
        ),
        SizedBox(
          child: Container(
            color: Colors.redAccent,
          ),
          height: 1,
        ),
        ListTile(
          trailing: Icon(FontAwesomeIcons.angleRight),
          title: Text('Zakładanie KEDA'),
          onTap: () => Navigator.of(context).pushNamed(KedScreen.id),
        ),
        SizedBox(
          child: Container(
            color: Colors.redAccent,
          ),
          height: 1,
        ),
        ListTile(
          trailing: Icon(FontAwesomeIcons.angleRight),
          title: Text('Wkłucie dożylne u dzieci'),
          onTap: () => Navigator.of(context).pushNamed(WklucieScreen.id),
        ),
        SizedBox(
          child: Container(
            color: Colors.redAccent,
          ),
          height: 1,
        ),
        ListTile(
          trailing: Icon(FontAwesomeIcons.angleRight),
          title: Text('Wkłucie doszpikowe'),
          onTap: () => Navigator.of(context).pushNamed(BigScreen.id),
        ),
        SizedBox(
          child: Container(
            color: Colors.redAccent,
          ),
          height: 1,
        ),
        ListTile(
          trailing: Icon(FontAwesomeIcons.angleRight),
          title: Text('Konikopunkcja'),
          onTap: () => Navigator.of(context).pushNamed(Konikopunkcja.id),
        ),
        SizedBox(
          child: Container(
            color: Colors.redAccent,
          ),
          height: 1,
        ),
        ListTile(
          trailing: Icon(FontAwesomeIcons.angleRight),
          title: Text('Odma prężna'),
          onTap: () => Navigator.of(context).pushNamed(Odma.id),
        ),
        SizedBox(
          child: Container(
            color: Colors.redAccent,
          ),
          height: 1,
        ),
        ListTile(
          trailing: Icon(FontAwesomeIcons.angleRight),
          title: Text('Defibrylacja u dzieci'),
          onTap: () => Navigator.of(context).pushNamed(Defibrilation.id),
        ),
        SizedBox(
          child: Container(
            color: Colors.redAccent,
          ),
          height: 1,
        ),
        ListTile(
          trailing: Icon(FontAwesomeIcons.angleRight),
          title: Text('Pedipac'),
          onTap: () => Navigator.of(context).pushNamed(Pedipac.id),
        ),
        SizedBox(
          child: Container(
            color: Colors.redAccent,
          ),
          height: 1,
        ),
      ],
    );
  }
}
