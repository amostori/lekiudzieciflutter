import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:leki_u_dzieci/src/constance.dart';
import 'package:leki_u_dzieci/src/screens/drugListScreen.dart';

class WeightScreen extends StatelessWidget {
  static const String id = 'WeightScreen';

  void selectPage(
      {BuildContext context, String wiek, String title, String weight}) {
    Navigator.pushReplacementNamed(
      context,
      DrugListScreen.id,
      arguments: {'wiek': wiek, 'title': title, 'weight': weight},
    );
  }

  @override
  Widget build(BuildContext context) {
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, String>;
    String wiekArgs = routeArgs['wiek'];
    String titleArgs = routeArgs['title'];
    String inputWeight = '0';
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                title: Text('Podaj wagę pacjenta',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                    )),
                background: Image(
                  image: AssetImage(
                    'images/apgar_kid.jpg',
                  ),
                  fit: BoxFit.cover,
                ),
              ),
            )
          ];
        },
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  kImageSizedBox,
                  Text(
                    'Waga dzieci w danym wieku może być różna. Tu możesz'
                    ' ją skorygować.',
                    textAlign: TextAlign.center,
                  ),
                  kImageSizedBox,
                  TextField(
                    onChanged: (text) => inputWeight = text,
                    onSubmitted: (value) => selectPage(
                      context: context,
                      title: titleArgs,
                      wiek: wiekArgs,
                      weight: value,
                    ),
                    autofocus: true,
                    maxLength: 3,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Tu wpisz wagę dziecka',
                      suffixIcon: IconButton(
                        icon: Icon(FontAwesomeIcons.angleRight),
                        onPressed: () => selectPage(
                          context: context,
                          title: titleArgs,
                          wiek: wiekArgs,
                          weight: inputWeight,
                        ),
                      ),
                    ),
                    style: TextStyle(fontSize: 18),
                  ),
                  kImageSizedBox,
                  kImageSizedBox,
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
